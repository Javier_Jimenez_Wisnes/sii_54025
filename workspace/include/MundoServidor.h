// Javier Jimenez Wisnes
// Mundo.h: interface for the CMundo class.


#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "Listaesferas.h"
#include "Listadisparos.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

class CMundo
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();

	void InitGL();
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();

	ListaDisparos listaD;
	ListaEsferas listaE;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int mififo;
	//int fifomun;
	//int fifotec;
	
	Socket accept;
	Socket comm;
	int sconnect;
	//int scomm;
	
	pthread_t tid;

	float time;
	bool fin;

	int puntos1;
	int puntos2;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
