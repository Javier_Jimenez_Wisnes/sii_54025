#include "Vector2D.h"
#include "Esfera.h"
#include <vector>
//using namespace std;

class ListaEsferas
{
public:
	std::vector <Esfera *> listae;
 
        ListaEsferas(){};
        virtual ~ListaEsferas(){for(int i=0;i<listae.size();i++) delete listae[i]; listae.clear();}

	void CrearEsfera(Esfera * e){listae.push_back(e);}
	int GetN(){return listae.size();}
        void Mueve(float t){for(int i=0;i<listae.size();i++) listae[i]->Mueve(t);}
        void Dibuja(){for(int i=0;i<listae.size();i++) listae[i]->Dibuja();}
	Esfera* operator[](int i){if(i>=listae.size()) i=listae.size()-1; if(i<0) i=0; return listae[i];}
};
