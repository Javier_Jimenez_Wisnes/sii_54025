#include "Vector2D.h"
#include "Disparo.h"
#include <vector>
//using namespace std;

class ListaDisparos
{
public:
        std::vector <Disparo *> listad;
 
        ListaDisparos(){};
        virtual ~ListaDisparos(){for(int i=0;i<listad.size();i++) delete listad[i]; listad.clear();}

        void CrearDisparo(Disparo * d){listad.push_back(d);}
	void EliminarDisparo(Disparo * d){for(int i=0;i<listad.size();i++) if(d==listad[i]) 
	listad.erase(listad.begin()+i);}

        int GetN(){return listad.size();}
        void Mueve(float t){for(int i=0;i<listad.size();i++) listad[i]->Mueve(t);}
        void Dibuja(){for(int i=0;i<listad.size();i++) listad[i]->Dibuja();}
        Disparo* operator[](int i){if(i>=listad.size()) i=listad.size()-1; if(i<0) i=0; return listad[i];}
};
