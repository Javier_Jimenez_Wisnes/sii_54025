
#pragma once
#include "glut.h"

#include "Vector2D.h"

class Disparo 
{
public:
        Disparo(float cx, float cy, float vx, float vy);

        virtual ~Disparo(){};

        Vector2D centro;
        Vector2D velocidad;
        float radio;

        void Mueve(float t);
        void Dibuja();
};

