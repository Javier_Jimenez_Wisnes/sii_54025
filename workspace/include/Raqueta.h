// Javier Jimenez Wisnes
// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
/*private:
	Vector2D velocidad;
	bool toque;
	int disparos;
	int maxdisparos;
	float tparo;
	float tiempo;*/
public:
	Raqueta();
	virtual ~Raqueta();
	//void SetVel(float x, float y);
	void Mueve(float t);
	//float Getxdis();
	//float Getydis();
	bool GetDisparos();
	void SetDisparo(){disparos++;}
	void ResetDisparos(){disparos=0; toque=false;}
	void Tocado(){toque=true;}
	
	Vector2D velocidad;
	bool toque;
	int disparos;
	int maxdisparos;
	float tparo;
	float tiempo;
};

