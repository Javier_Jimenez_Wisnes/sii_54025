// Javier Jimenez Wisnes
// Mundo.cpp: implementation of the CMundo class.

#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/wait.h>

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	int fdcontrol;
	
	fdcontrol=close(mififo);
	if(fdcontrol<0){
        	perror("Error en cerrado de mififo");
        	return;
	}
	
}

void captura(int s){
	printf("Recepción de la señal %s\n", strsignal(s));
	if(s == 12){
		//printf("Señal de terminacion estandar %d\n", s);
		exit(0);
	}
	else{
		//printf("Señal de terminacion excepcional %d\n", s);
		exit(s);
	}
}

void* hilo_comandos(void* d){
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
      return p;
}

void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];

            
            comm.Receive(cad,sizeof(cad));

            
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	listaE.Dibuja();
	listaD.Dibuja();

	//		AQUI TERMINA MI DIBUJO

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{

	if(time<15.0f)
		time+=0.025f;

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	listaE.Mueve(0.025f);
	listaD.Mueve(0.025f);
	int i;
   for(i=0;i<paredes.size();i++)
	{
		for(int j=0;j<listaE.GetN();j++)
 		  paredes[i].Rebota(*listaE[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

//Choques listaesferas
   for(i=0;i<listaE.GetN();i++){

	int jug[2];
	int controlW;

	jugador1.Rebota(*listaE[i]);
	jugador2.Rebota(*listaE[i]);
	if(fondo_izq.Rebota(*listaE[i]))
	{
		listaE[i]->centro.x=0;
		listaE[i]->centro.y=rand()/(float)RAND_MAX;
		listaE[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
		listaE[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
		listaE[i]->radio=listaE[i]->radio-0.05;

 		puntos2++;
		jug[0]=2;
		jug[1]=puntos2;
		controlW=write(mififo, jug, sizeof(jug));
		
		if(controlW<0){
        	perror("Error en escritura de mififo de jugador 2");
        	return;
		}
		
		jugador1.ResetDisparos();
		jugador2.ResetDisparos();
		//if(puntos2+puntos1==3) listaE.CrearEsfera(new Esfera);

	}

	if(fondo_dcho.Rebota(*listaE[i]))
	{
		listaE[i]->centro.x=0;
		listaE[i]->centro.y=rand()/(float)RAND_MAX;
		listaE[i]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
		listaE[i]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
		listaE[i]->radio=listaE[i]->radio-0.05;

		puntos1++;
		jug[0]=1;
                jug[1]=puntos1;
		controlW=write(mififo, jug, sizeof(jug));
		
		if(controlW<0){
        	perror("Error en escritura de mififo de jugador 1");
        	return;
		}

		jugador1.ResetDisparos();
		jugador2.ResetDisparos();
		//if(puntos2+puntos1==3) listaE.CrearEsfera(new Esfera);

	}
    }

//Lista disparos
for(i=0;i<listaD.GetN();i++){
        if(jugador1.Choque(*listaD[i])){
		jugador1.Tocado();
		listaD.EliminarDisparo(listaD[i]);
	}
        if(jugador2.Choque(*listaD[i])){
                jugador2.Tocado();
		listaD.EliminarDisparo(listaD[i]);
	}
	if(fondo_izq.Choque(*listaD[i]))
		listaD.EliminarDisparo(listaD[i]);
	if(fondo_dcho.Choque(*listaD[i]))
                listaD.EliminarDisparo(listaD[i]);

}

//Salida por puntos
if(puntos1>3 || puntos2>3){
	//fin = true;
	exit(0);
	}
	
//Escribir en FIFO a cliente la información de cada esfera
	char cad[200];	
	
	for(int j = 0; j < listaE.GetN(); j++)
		sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d",listaE[j]->centro.x,listaE[j]->centro.y,jugador1.x1,jugador1.y1,
		jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
		
	comm.Send(cad,sizeof(cad));

	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y){

}

void CMundo::Init()
{
	listaE.CrearEsfera(new Esfera);

//Tratamiento de FIFOs
	mififo = open("/tmp/mififo1", O_WRONLY);
	if(mififo<0){
		perror("No se ha podido abrir la tubería con logger mififo");
		return;
	}
	
//Tratamiento sockets
	accept.InitServer("127.0.0.1",4200);
	comm = accept.Accept();
	char cad[25];
	comm.Receive(cad,sizeof(cad));
	printf("Nombre del cliente: %s\n", cad);
		
	
//Tratamiento de thread
	int threadcontrol;
	threadcontrol=pthread_create(&tid, NULL, hilo_comandos, this);
	if (threadcontrol!=0) {
		perror("Error creacion del pthread");
		return;
	}

//Inicio contador
        time=0.0f;
        fin=false;



	Plano p;

//Inicio señales
	struct sigaction acc;
        acc.sa_handler = captura;
        acc.sa_flags=0;
        sigemptyset(&acc.sa_mask);
        sigaction(SIGINT, &acc, NULL);
        sigaction(SIGTERM, &acc, NULL);
        sigaction(SIGPIPE, &acc, NULL);
        sigaction(SIGUSR2, &acc, NULL);
	
//Pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//Pared superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

