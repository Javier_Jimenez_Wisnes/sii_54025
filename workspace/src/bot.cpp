#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <iostream>

#include "DatosMemCompartida.h"

int main(void)
{
	int memory, control1, control2, control3;
	DatosMemCompartida *prelation;
	
	memory = open("/tmp/memory1", O_RDWR);
	if (memory<0)
	{
		perror("Error de apertura de buffer");
		exit(1);
	}

	prelation = static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, memory, 0));
	if (prelation == MAP_FAILED)
	{
		perror("Error de proyeccion de memoria");
		close(memory);
		return 1;
	}

	control1= close(memory);
	if(control1==-1){
        	perror("Error en cerrado de fichero");
        	return 1;
	}

	while(1)
	{
		
		//Raqueta 1
		if (prelation->esfera.centro.y > ((prelation->raqueta1.y1 + prelation->raqueta1.y2) / 2))
			prelation->accion = -1;
		if (prelation->esfera.centro.y < ((prelation->raqueta1.y1 + prelation->raqueta1.y2) / 2))
			prelation->accion = 1;
		if (prelation->esfera.centro.y == ((prelation->raqueta1.y1 + prelation->raqueta1.y2) / 2))
			prelation->accion = 0;
		
		//Raqueta 2
		if (prelation->esfera.centro.y > ((prelation->raqueta1.y1 + prelation->raqueta1.y2) / 2))
                        prelation->accion2 = -1;
                if (prelation->esfera.centro.y < ((prelation->raqueta1.y1 + prelation->raqueta1.y2) / 2))
                        prelation->accion2 = 1;
                if (prelation->esfera.centro.y == ((prelation->raqueta1.y1 + prelation->raqueta1.y2) / 2))
                        prelation->accion2 = 0;
		
		
		if(prelation->fin==true)
			break;

		usleep(25000);
	}

	control2=munmap(prelation, sizeof(DatosMemCompartida));
	if(control2==-1){
        	perror("Error en desproyección de memoria");
        	return 1;
	}
	
	control3=unlink("/tmp/memory1");
	if(control3==-1){
        	perror("Error en el desenlace con memory1");
        	return 1;
	}
	return 0;
}
