#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <string>
#include <iostream>

using namespace std;

int main(void)
{
	int mififo;
	int jug[2];
	int controlR,controlC,controlU,controlM;

	controlM=mkfifo("/tmp/mififo1", 0666);
	
	if(controlM<0)							//Creacion FIFO
	{
		perror("No se ha podido crear la tubería.");
		return -1;
	}
	
	mififo = open("/tmp/mififo1", O_RDONLY);
	if(mififo<0)							//Apertura FIFO
	{
		perror("No se ha podido abrir la tubería.");
		return -1;
	}
	
	while(controlR=read(mififo, jug, sizeof(jug))){				//Lectura hasta cerrar FIFO
		if(controlR<0){
		perror("Error en la lectura del fifo");
		return -1;
		}
		if(controlR > 0 && controlR != sizeof(jug)){
		perror("Tamaño recibido erroneo.");
		return -1;
		}
		cout << "Jugador " << jug[0] << " marca 1 punto, lleva un total de " << jug[1] << " puntos.\n";
	}	
		
	
	controlC=close(mififo);					//Cerrado de FIFO
	if(controlC<0)							
	{
		perror("No se ha podido cerrar la tubería.");
		return -1;
	}
	
	
	controlU=unlink("/tmp/mififo1");				//Desenlazado de FIFO
	if(controlU<0)							
	{
		perror("No se ha podido desenlazar tuberia");
		return -1;
	}
	return 0;
}
