#include "Disparo.h"
#include "glut.h"

Disparo::Disparo(float cx, float cy, float vx, float vy){
	centro.x=cx;
	centro.y=cy;
	velocidad.x=vx;
	velocidad.y=vy;
	radio=0.15;
}

void Disparo::Dibuja()
{
        glColor3ub(100,50,100);
        glEnable(GL_LIGHTING);
        glPushMatrix();
        glTranslatef(centro.x,centro.y,0);
        glutSolidSphere(radio,15,15);
        glPopMatrix();
}

void Disparo::Mueve(float t)
{
centro.x=centro.x+velocidad.x*t;
centro.y=centro.y+velocidad.y*t;
}

