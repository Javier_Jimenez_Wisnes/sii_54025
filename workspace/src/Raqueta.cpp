// Javier Jimenez Wisnes
// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	disparos=0;
	maxdisparos=2;
	toque=false;
	tparo=2.0f;
	tiempo=0.0f;
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	if(toque){
	  velocidad.x=0.0f;
	  velocidad.y=0.0f;
	  tiempo+=t;
	  if(tiempo>=tparo){
	    toque=false;
	    tiempo=0.0f;
	  }
	}
	else{
	x1=x1+velocidad.x*t;
	x2=x2+velocidad.x*t;
	y1=y1+velocidad.y*t;
	y2=y2+velocidad.y*t;
	}
}

/*void Raqueta::SetVel(float x, float y){
	velocidad.x=x;
	velocidad.y=y;
}

float Raqueta::Getxdis(){
	float res;
	res=((x1+x2)/2);
	return res;
}

float Raqueta::Getydis(){
	float res;
	res=((y1+y2)/2);
	return res;
}*/

bool Raqueta::GetDisparos(){
	if(disparos<maxdisparos) return true;
	else return false;
}
